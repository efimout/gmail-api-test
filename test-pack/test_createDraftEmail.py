import MessageHelper
import LoginHelper

SCOPES = 'https://mail.google.com/'


class TestCreateDraftEmail():
    print 'Given I am an authenticated user'
    service = LoginHelper.login()
    sender = 'me'
    to = 'efi.testingapi@gmail.com'
    subject = 'Test subject'
    mainMessage = 'I am a draft message'
    numberOfDraftsBefore = MessageHelper.ListMessagesWithLabels(service, 'me', ['DRAFT'])

    print 'When I create a draft message'
    message = MessageHelper.CreateMessage(sender, to, subject, mainMessage)
    draft = MessageHelper.CreateDraft(service, sender, message)

    print 'Then I see this message appearing first in the list of draft messages'
    numberOfDraftsAfter = MessageHelper.ListMessagesWithLabels(service, 'me', ['DRAFT'])
    draftMessage = numberOfDraftsAfter[0]

    assert len(numberOfDraftsAfter) == len(numberOfDraftsBefore) + 1
    assert draftMessage['id'] == draft['message']['id']

    MessageHelper.DeleteMessage(service, 'me', draftMessage['id'])
