import MessageHelper
import LoginHelper
import time

SCOPES = 'https://mail.google.com/'


class TestSendEmail():
    print 'Given I am an authenticated user'
    service = LoginHelper.login()
    sender = 'me'
    to = 'efi.testingapi@gmail.com'
    subject = 'Test subject'
    mainMessage = 'Hello all'
    numberOfSentMessagesBefore = MessageHelper.ListMessagesWithLabels(service, 'me', ['SENT'])
    inboxMessagesBefore = MessageHelper.ListMessagesWithLabels(service, 'me', ['INBOX'])

    print 'When I create a message'
    message = MessageHelper.CreateMessage(sender, to, subject, mainMessage)

    print 'And I send the message'
    messageSent = MessageHelper.SendMessage(service, sender, message)

    print 'Then I see this message appearing in the list of sent messages'
    numberOfSentMessagesAfter = MessageHelper.ListMessagesWithLabels(service, 'me', ['SENT'])
    inboxMessagesAfter = MessageHelper.ListMessagesWithLabels(service, 'me', ['INBOX'])
    messageReceived = MessageHelper.GetMessage(service, sender, messageSent['id'])

    assert len(numberOfSentMessagesAfter) == len(numberOfSentMessagesBefore) + 1
    assert len(inboxMessagesAfter) == len(inboxMessagesBefore) + 1
    assert messageReceived['id'] == messageSent['id']
    assert messageReceived['snippet'] == mainMessage
    assert messageReceived['payload']['headers'][4]['name'] == 'to'
    assert messageReceived['payload']['headers'][4]['value'] == to

    time.sleep(3)
    MessageHelper.DeleteMessage(service, 'me', messageSent['id'])

